package monitor;

public interface IAssunto {
    void adicionarObservador(Observador observador);

    boolean notificar() throws Exception;
}
