package monitor;

public abstract class Observador {
    public abstract void update(String mensagem);
}
