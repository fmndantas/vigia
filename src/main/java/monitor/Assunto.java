package monitor;

import java.util.ArrayList;
import java.util.List;

public class Assunto implements IAssunto {
    private final List<Observador> observadores = new ArrayList<>();

    public void adicionarObservador(Observador observer) {
        this.observadores.add(observer);
    }

    public boolean notificar() throws Exception {
        var mensagem = obterMensagem();
        this.observadores.forEach(observer -> observer.update(mensagem));
        return true;
    }

    public String obterMensagem() throws Exception {
        throw new Exception("A classe base não implementa a mensagem");
    }
}
