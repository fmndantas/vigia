package monitor;

public class ObservadorConsole extends Observador {
    public void update(String mensagem) {
        System.out.println(mensagem);
    }
}
