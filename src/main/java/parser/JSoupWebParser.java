package parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.regex.Pattern;

public class JSoupWebParser implements IWebParser {
    public Document obterDocumento(String url) throws Exception {
        return Jsoup
                .connect(url)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0")
                .get();
    }

    public String stringHtmlPeloSelect(String select, String uri) throws Exception {
        return obterDocumento(uri).select(select).html();
    }

    public String stringHtmlPeloSelectRegexGrupoUnico(String select, String uri, String expressao) throws Exception {
        var naoFiltrado = this.stringHtmlPeloSelect(select, uri);
        var matcher = Pattern
                .compile("(" + expressao + ")")
                .matcher(naoFiltrado);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }

    public double string2Double(String valorDouble) throws Exception {
        valorDouble = valorDouble
                .replace(".", "")
                .replace(",", ".");
        try {
            return Double.parseDouble(valorDouble);
        } catch (Exception ex) {
            throw new Exception("Não foi possível converter o seguinte valor em double: " + valorDouble + ". " + ex.getMessage());
        }
    }
}
