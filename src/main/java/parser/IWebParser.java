package parser;

public interface IWebParser {
    String stringHtmlPeloSelect(String select, String uri) throws Exception;

    String stringHtmlPeloSelectRegexGrupoUnico(String select, String uri, String expressao) throws Exception;

    double string2Double(String valorDouble) throws Exception;
}
