import monitor.ObservadorConsole;
import parser.JSoupWebParser;
import vigia.VigiaAmericanasMudanca;

public class Runner {
    public static void main(String[] args) throws Exception {
        var americanasFogao = "https://www.americanas.com.br/produto/1838932936?cor=INOX&voltagem=BIVOLT";
        var magazineFogao = "https://www.magazineluiza.com.br/fogao-4-bocas-a-gas-electrolux-52lxs-acendimento-automatico-silver-bivolt/p/kag7b44a9k/ed/fogo/?&seller_id=mega-mamute&utm_source=google&utm_medium=pla&utm_campaign=&partner_id=54222&gclid=EAIaIQobChMI4PeKooPD8wIVAeOzCh1_MAR0EAQYASABEgLdHPD_BwE&gclsrc=aw.ds";

        var vigia = new VigiaAmericanasMudanca(americanasFogao, new JSoupWebParser());

        vigia.adicionarObservador(new ObservadorConsole());

        vigia.notificar();
    }
}
