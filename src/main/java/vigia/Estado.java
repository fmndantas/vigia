package vigia;

public class Estado {
    private String nome;
    private String link;
    private double preco;
    private double desconto;
    private boolean atualizacaoCorreta;

    public Estado() {

    }

    public Estado(double preco) {
        this.preco = preco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public boolean isAtualizacaoCorreta() {
        return atualizacaoCorreta;
    }

    public void setAtualizacaoCorreta(boolean atualizacaoCorreta) {
        this.atualizacaoCorreta = atualizacaoCorreta;
    }
}
