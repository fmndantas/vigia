package vigia;

import parser.IWebParser;

public abstract class VigiaComEstado extends Vigia {
    protected Estado estadoAtual;
    protected Estado estadoPassado;

    VigiaComEstado(String uri, IWebParser parser) {
        super(uri, parser);
        estadoAtual = new Estado(SEM_PRECO);
        estadoPassado = new Estado(SEM_PRECO);
    }

    protected boolean deveNotificar() {
        return super.deveNotificar();
    }

    protected void atualizarEstadoPassado() {
        estadoPassado.setNome(estadoAtual.getNome());
        estadoPassado.setDesconto(estadoAtual.getDesconto());
        estadoPassado.setPreco(estadoAtual.getPreco());
        estadoPassado.setLink(estadoAtual.getLink());
    }

    protected void atualizarEstadoAtual() {
        try {
            estadoAtual.setNome(obterNome());
            estadoAtual.setLink(uri);
            estadoAtual.setPreco(obterPreco());
            estadoAtual.setDesconto(obterDesconto());
            estadoAtual.setAtualizacaoCorreta(true);
        } catch (Exception ex) {
            estadoAtual.setAtualizacaoCorreta(false);
        }
    }

    protected void antesNotificarFaca() {
        atualizarEstadoAtual();
    }

    protected void sePuderNotificarFacaDepoisNotificar() {
        atualizarEstadoPassado();
    }
}
