package vigia;

import parser.IWebParser;

import java.util.regex.Pattern;

public class VigiaAmericanas extends VigiaComEstado {
    private final String selectPreco = "#rsyswpsdk > div > main > div.src__Container-dda50e-0.ihwNf > div.product-offer__Container-sc-1xm718r-0.knpcH > div.src__Wrapper-sc-1urqdh5-0.krjUEV > div.src__PriceWrapper-sc-1jvw02c-4.gxILPN > div";
    private final String selectNome = "#rsyswpsdk > div > main > div.src__Container-dda50e-0.ihwNf > div.product-info__ProductInfoContainer-sc-1u2zqg7-3.faPlbO > div:nth-child(2) > h1";

    public VigiaAmericanas(String uri, IWebParser parser) {
        super(uri, parser);
    }

    public String obterNome() throws Exception {
        return this.parser.stringHtmlPeloSelect(selectNome, uri);
    }

    public double obterPreco() throws Exception {
        var html = this.parser.stringHtmlPeloSelect(selectPreco, super.uri);
        var matcher = Pattern.compile("([\\d.]*\\d{1,3},\\d{2})").matcher(html);
        if (matcher.find()) {
            var precoString = matcher
                    .group(0)
                    .replace(".", "")
                    .replace(",", ".");
            try {
                return Double.parseDouble(precoString);
            } catch (Exception ex) {
                throw new Exception("Problema para converter preço buscado. String é: " +
                        precoString + ". " + ex.getMessage(), ex);
            }
        }
        return SEM_PRECO;
    }

    public double obterDesconto() {
        return 0;
    }

    protected boolean deveNotificar() {
        return super.deveNotificar() && estadoAtual.isAtualizacaoCorreta();
    }

    public String obterMensagem() throws Exception {
        var novaLinha = "\n";
        return "Nome: " +
                estadoAtual.getNome() +
                novaLinha +
                "Preço: R$ " +
                estadoAtual.getPreco() +
                novaLinha +
                "Link: " +
                estadoAtual.getLink();
    }
}
