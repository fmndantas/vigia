package vigia;

import parser.IWebParser;

public class VigiaAmericanasMudanca extends VigiaAmericanas {
    public VigiaAmericanasMudanca(String uri, IWebParser parser) {
        super(uri, parser);
    }

    @Override
    public boolean deveNotificar() {
        if (!super.deveNotificar())
            return false;
        if (estadoAtual.getPreco() == SEM_PRECO)
            return false;
        return estadoPassado.getPreco() == SEM_PRECO ||
                estadoAtual.getPreco() != estadoPassado.getPreco();
    }

    @Override
    public String obterMensagem() throws Exception {
        if (estadoPassado.getPreco() == SEM_PRECO)
            return super.obterMensagem();
        String transicao;
        if (estadoAtual.getPreco() > estadoPassado.getPreco()) {
            transicao = ":( Aumento";
        } else {
            transicao = ":) Diminuição";
        }
        return super.obterMensagem() +
                "\n" +
                transicao + " de preço => Antes: " + estadoPassado.getPreco() + "/Depois: " + estadoAtual.getPreco();
    }
}
