package vigia;

import monitor.Assunto;
import parser.IWebParser;

public abstract class Vigia extends Assunto {
    protected double SEM_PRECO = -1.0;

    protected IWebParser parser;
    protected String uri;

    Vigia(String uri, IWebParser parser) {
        this.parser = parser;
        this.uri = uri;
    }

    protected boolean deveNotificar() {
        return true;
    }

    protected void sePuderNotificarFacaDepoisNotificar() {
    }

    protected void antesNotificarFaca() {
    }

    protected abstract String obterNome() throws Exception;

    protected abstract double obterPreco() throws Exception;

    protected abstract double obterDesconto() throws Exception;

    public boolean notificar() throws Exception {
        antesNotificarFaca();
        if (deveNotificar()) {
            var notificou = super.notificar();
            sePuderNotificarFacaDepoisNotificar();
            return notificou;
        }
        return false;
    }
}
