import parser.IWebParser;
import vigia.VigiaAmericanasMudanca;

public class VigiaAmericanasMudancaMock extends VigiaAmericanasMudanca {
    public double mockPreco;
    public double mockDesconto;
    public String mockNome;

    public VigiaAmericanasMudancaMock(String uri, IWebParser parser) {
        super(uri, parser);
        mockPreco = SEM_PRECO;
        mockDesconto = SEM_PRECO;
        mockNome = "mockNome";
    }

    public double obterPreco() {
        return mockPreco;
    }

    public String obterNome() {
        return mockNome;
    }

    public double obterDesconto() {
        return mockDesconto;
    }
}
