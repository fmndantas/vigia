import org.junit.Assert;
import org.junit.Test;
import parser.JSoupWebParser;

public class TestVigiaMagazineMudanca {
    @Test
    public void NotificarSoQuandoAlterar() throws Exception {
        var SEM_PRECO = -1.0;

        var vigia = new VigiaAmericanasMudancaMock("", new JSoupWebParser());

        vigia.mockPreco = SEM_PRECO;
        Assert.assertFalse(vigia.notificar());

        vigia.mockPreco = 999.99;
        Assert.assertTrue(vigia.notificar());

        vigia.mockPreco = 999.98;
        Assert.assertTrue(vigia.notificar());

        Assert.assertFalse(vigia.notificar());

        vigia.mockPreco = SEM_PRECO;
        Assert.assertFalse(vigia.notificar());

        vigia.mockPreco = 999.98;
        Assert.assertFalse(vigia.notificar());

        vigia.mockPreco = 1000.99;
        Assert.assertTrue(vigia.notificar());
    }
}
